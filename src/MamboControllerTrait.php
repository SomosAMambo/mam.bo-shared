<?php namespace Mambo;

use App\Http\Requests;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

/**
 * Trait MamboControllerTrait
 * @package SomosAMambo\mam.bo-shared
 */
trait MamboControllerTrait
{
    /**
     * Os métodos neste controlador são permitidos somente para usuários autenticados.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Recupera o nome da classe deste controlador.
     *
     * @param bool $replaceControllerString
     * @return String
     */
    public function getControllerName($replaceControllerString = false)
    {
        $className = class_basename(get_class($this));
        if ($replaceControllerString) {
            return str_replace('Controller', '', $className);
        }
        return $className;
    }

    /**
     * Recupera as propriedades do modelo e soma a array $params.
     *
     * @return mixed Array
     * @internal param $params
     */
    public function getViewConfiguration($trashed = false)
    {
        $model = new $this->model;
        $table = $model->getTable();
        $request = request();
        $cellsJson = [];
        if (!$trashed) {
            $cells = array_merge($model->cells, [
                "$table.created_at" => ['data' => 'created_at'],
                "$table.updated_at" => ['data' => 'updated_at'],
            ]);
        } else {
            $cells = array_merge($model->cells, [
                "$table.deleted_at" => ['data' => 'deleted_at'],
            ]);
        }
        foreach ($cells as $key => $cell) {
            if (is_array($cell)) {
                array_push($cellsJson, array_merge($cell, ['name' => $key]));
            } else {
                array_push($cellsJson, ['name' => $cell, 'data' => $cell]);
            }
        }
        return [
            'headers' => $model->headers,
            'cells' => $model->cells,
            'cellsJson' => json_encode($cellsJson),
            'controller' => $this->getControllerName(),
            'controllerName' => $this->getControllerName(true),
            'plural' => $model->plural,
            'singular' => $model->singular,
            'model' => $this->model,
            'tabbed' => $model->tabbed,
            'rules' => $model->getRules(),
            'breadcrumbs' => $this->getBreadcrumbs(),
        ];
    }

    /**
     * Traduz os links das breadcrumbs e retorna o label e a URI
     *
     * @return mixed Array
     */
    public function getBreadcrumbs()
    {
        $model = new $this->model;
        $request = request();
        $breadcrumbs = [
            'create' => ['Novo', false],
            'edit' => ['Edição', false],
            'duplicate' => ['Novo baseado em modelo', false],
        ];
        if (isset($model->breadcrumbs)) {
            $breadcrumbs = array_merge($model->breadcrumbs, $breadcrumbs);
        }
        $segments = [];
        $url = '';
        for ($i = 0; $i <= count($request->segments()); $i++) {
            $url .= '/' . $request->segment($i);
            $segment = [];
            if (isset($breadcrumbs[$request->segment($i)])) {
                $segment['text'] = $breadcrumbs[$request->segment($i)][0];
                if ($breadcrumbs[$request->segment($i)][1]) {
                    $segment['url'] = url(substr($url, 1));
                }
                array_push($segments, $segment);
            }
        }
        return $segments;
    }

    /**
     * Mostra view para receber dados ajax na datatable.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->checkPermission('listar');
        return view('__mambo.datatable',
            $this->getViewConfiguration()
        );
    }

    /**
     * Mostra view para receber dados ajax na datatable.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function trash(Request $request)
    {
        return view('__mambo.datatableTrash',
            $this->getViewConfiguration(true)
        );
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        $model = new $this->model;
        $table = $model->getTable();
        $query = $model->select("$table.*");
        if (method_exists($model, 'dataTableQueryHook')) {
            $query = $model->dataTableQueryHook($query);
        }
        if (isAssoc($model->cells)) {
            if (is_array(reset($model->cells))) {
                $firstColumn = reset($model->cells)['data'];
            } else {
                $firstColumn = reset($model->cells);
            }
        } else {
            $firstColumn = $model->cells[0];
        }
        $dt = Datatables::of($query)
            ->setRowId('id')
            ->editColumn($firstColumn,
                '<a href="{{ action(\'' . $this->getControllerName() . '@edit\',  $id) }}">{{ $' . $firstColumn .
                ' }}</a>')
            ->editColumn('created_at', function ($row) {
                if ($row->created_at->diffInHours() > 24) {
                    return $row->created_at->format("d/m/y H\hi");
                } else {
                    return $row->created_at->diffForHumans();
                }
            })
            ->editColumn('updated_at', function ($row) {
                if ($row->updated_at->diffInHours() > 24) {
                    return $row->updated_at->format("d/m/y H\hi");
                } else {
                    return $row->updated_at->diffForHumans();
                }
            });
        if (method_exists($model, 'columnEditHook')) {
            $dt = $model->columnEditHook($dt);
        }
        return $dt->make(true);
    }

    /**
     * Process datatables ajax request, just trashed.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyDataTrash()
    {
        $model = new $this->model;
        $table = $model->getTable();
        $query = $model->onlyTrashed()->select("$table.*");
        if (method_exists($model, 'dataTableQueryHook')) {
            $query = $model->dataTableQueryHook($query);
        }
        $dt = Datatables::of($query)
            ->setRowId('id')
            ->editColumn('deleted_at', function ($row) {
                if ($row->deleted_at->diffInHours() > 24) {
                    return $row->deleted_at->format("d/m/y H\hi");
                } else {
                    return $row->deleted_at->diffForHumans();
                }
            });
        if (method_exists($model, 'columnEditHook')) {
            $dt = $model->columnEditHook($dt);
        }
        return $dt->make(true);
    }

    /**
     * Mostra um único.
     *
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('ver');
        $model = new $this->model;
        return $model->findOrFail($id);
    }

    /**
     * Mostra a página para criar um novo.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('criar');
        $model = new $this->model;
        return view(isset($model->createViewFile) ? $model->createViewFile : '_generic.create',
            $this->getViewConfiguration()
        );
    }

    /**
     * Salva um novo.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('salvar');

        $model = new $this->model;

        //Validação baseada nas propriedades da classe filha.
        //Caso a requisição for ajax e a validação falhar é retornado um json automaticamente com a coleção de erros.
        //Caso seja um post normal, no caso da validação falhar, ele retorna para a rota anterior com uma coleção de erros
        //e os dados do request para povoar o form.
        $this->validate($request, $model->getRules());

        //Persistindo os dados do request no banco.
        $record = $model->create($request->all());

        //Persistindo as relações ManyToMany.
        $record->syncManyToMany();

        //Via ajax retornamos o objeto criado em formato json.
        if ($request->ajax()) {
            return response()->json($record);
        }
        //Via post redirecionamos para a view de índice e com uma variável de sessão com uma mensagem.
        //Esses argumentos são aproveitados pela view layouts.app para exibir um componente javascript.
        return redirect()->action($this->getControllerName() . '@index')
            ->with([
                'flash_type' => 'success',
                'flash_title' => 'Sucesso!',
                'flash_message' => 'Novo ' . ucwords($model->singular) . ' adicionado com sucesso!',
                'flash_timeout' => 3,
            ]);
    }

    /**
     * Mostra a página para edição.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('editar');
        //Instaciando um modelo.
        $model = new $this->model;
        //Busca o registro no banco e devolve uma excessão Illuminate\Database\Eloquent\ModelNotFoundException
        //caso não encontre.
        $record = $model->findOrFail($id);
        return view(isset($model->editViewFile) ? $model->editViewFile : '_generic.edit',
            array_merge(
                ['data' => $record],
                $this->getViewConfiguration()
            )
        );
    }

    /**
     * Mostra a página para criação já povoada.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function duplicate($id, Request $request)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('criar');
        //Instaciando um modelo.
        $model = new $this->model;
        //Busca o registro no banco e devolve uma excessão Illuminate\Database\Eloquent\ModelNotFoundException
        //caso não encontre.
        $record = $model->findOrFail($id);
        return view('_generic.create',
            array_merge(
                ['data' => $record],
                $this->getViewConfiguration(),
                $record->idsRelacionados(),
                $model->optsCreateView
            )
        );
    }

    /**
     * Mostra a view para impressão do model
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function forPrint($id, Request $request)
    {
        //Instaciando um modelo.
        $model = new $this->model;
        //Busca o registro no banco e devolve uma excessão Illuminate\Database\Eloquent\ModelNotFoundException
        //caso não encontre.
        $record = $model->findOrFail($id);
        return view($model->printViewFile,
            array_merge(
                ['data' => $record],
                $this->getViewConfiguration(),
                $record->idsRelacionados(),
                $model->optsPrintView
            )
        );
    }

    /**
     * Mostra a view para impressão do model
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function pdf($id, Request $request)
    {
        //Instaciando um modelo.
        $model = new $this->model;
        //Busca o registro no banco e devolve uma excessão Illuminate\Database\Eloquent\ModelNotFoundException
        //caso não encontre.
        $record = $model->findOrFail($id);
        return \PDF::loadView($model->printViewFile,
            array_merge(
                ['data' => $record],
                $this->getViewConfiguration(),
                $record->idsRelacionados(),
                $model->optsPrintView
            )
        )->stream(camel_case($model->singular) . '.pdf');
    }

    /**
     * Atualiza.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('atualizar');
        //Instaciando um modelo.
        $model = new $this->model;
        //Validação. Ver método store.
        $this->validate($request, $model->getRules());
        //Busca o registro no banco e devolve uma excessão Illuminate\Database\Eloquent\ModelNotFoundException
        //caso não encontre.
        $record = $model->findOrFail($id);
        //Persiste no banco as alterações que vieram no request.
        $record->update($request->all());
        //Persistindo as relações ManyToMany.
        $record->syncManyToMany();
        //Via ajax retornamos o objeto criado em formato json.
        if ($request->ajax()) {
            return response()->json($record);
        }
        //Via post redirecionamos para a view de índice. Ver método store.
        return redirect()->action($this->getControllerName() . '@index')
            ->with([
                'flash_type' => 'success',
                'flash_title' => 'Sucesso!',
                'flash_message' => ucwords($model->singular) . ' alterado com sucesso!',
                'flash_timeout' => 3,
            ]);
    }

    /**
     * Deleta.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $model = new $this->model;
        if (method_exists($model, 'withTrashed')) { //Usa softdeletes
            $record = $model->withTrashed()->findOrFail($id);
            if (!$record->trashed()) {
                // Verifica se o usuário pode realizar a ação.
                $this->checkPermission('excluir');
                $record->delete();
            } else {
                // Verifica se o usuário pode realizar a ação.
                $this->checkPermission('limpar');
                $record->forceDelete();
            }
        } else { //NÃO Usa softdeletes
            $record = $model->findOrFail($id);
            // Verifica se o usuário pode realizar a ação.
            $this->checkPermission('excluir');
            $record->delete();
        }

        if ($request->ajax()) {
            return 'Removido com sucesso!';
        }
        return redirect()->action($this->getControllerName() . '@index')
            ->with([
                'flash_type' => 'success',
                'flash_title' => 'Sucesso!',
                'flash_message' => ucwords($model->singular) . ' removido com sucesso!',
                'flash_timeout' => 3,
            ]);
    }

    /**
     * Recupera.
     *
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function restore($id, Request $request)
    {
        // Verifica se o usuário pode realizar a ação.
        $this->checkPermission('restaurar');
        $model = new $this->model;
        $model->onlyTrashed()->findOrFail($id)->restore();
        if ($request->ajax()) {
            return 'Restaurado com sucesso!';
        }
        return redirect()->action($this->getControllerName() . '@index')
            ->with([
                'flash_type' => 'success',
                'flash_title' => 'Sucesso!',
                'flash_message' => ucwords($model->singular) . ' restaurado com sucesso!',
                'flash_timeout' => 3,
            ]);
    }

    /** TODO
     * Essa função checa se um usuário pode realizar determinada ação baseado no nome da ação
     * @param  string $action nome da action no controller
     * @return boolean retorna uma página de erro caso o usuário não ternha permissão
     */
    public function checkPermission($action)
    {
        if (isset($this::$permissions)) {
            if (auth()->user()->cannot($this::$permissions['permission'][$action])) {
                abort(403);
            }
        }
        return true;
    }
}