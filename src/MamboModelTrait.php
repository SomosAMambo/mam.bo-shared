<?php namespace Mambo;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Trait MamboControllerTrait 
 * @package SomosAMambo\mam.bo-shared
 */
trait MamboModelTrait
{
    /**
     * Usando revisionable para controle de revisões e blames: https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;

    protected $revisionEnabled = true;
    protected $revisionCreationsEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.

    /**
     * Usando uma trait de pesquisa: https://github.com/nicolaslopezj/searchable
     */
    use SearchableTrait;

    /**
     * Usando soft delete: http://laravel.com/docs/5.1/eloquent#soft-deleting
     */
    use SoftDeletes;

    public function getStatusLabelAttribute()
    {
        $codigoStatus = $this->status;
        if (empty($codigoStatus)) {
            $codigoStatus = 0;
        }
        $status = colecoes('statusContratos')[$codigoStatus];
        switch ($status) {
            case 'Inativo':
                $class = "warning";
                $icone = "thumbs-down";
                break;
            case 'Ativo':
                $class = "primary";
                $icone = "thumbs-up";
                break;
            case 'Suspenso':
                $class = "info";
                $icone = "pause";
                break;
            case 'Cancelado':
                $class = "danger";
                $icone = "ban";
                break;
        }
        return "<span class=\"label label-{$class}\"><i class=\"fa fa-{$icone}\"></i> {$status}</span>";
    }

    public function getLinksAttribute()
    {
        return [
            'edit' => action(ucfirst($this->getTable()) . 'Controller@edit', $this->id)
        ];
    }

    /**
     * Scope status ativo
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAtivo($query)
    {
        if (\Schema::hasColumn($this->getTable(), 'status')) {
            return $query
                ->where('status', 1);
        }
    }

    /**
     * Realizaça a associação polimórfica entre os modelos que possuem a funcionalidade de
     * auditoria  - RevisionableTrait
     */
    public function Revisions()
    {
        return $this->morphMany('App\Revision', 'revisionable');
    }

    /**
     * Persiste as relações muitos para muitos na tabela pivot. Utiliza-se uma propriedade definida na classe filha. O nome deve
     * ser, obrigatoriamente, o nome plural do Modelo relacionado. Nas views será o <plural>  concatenado com 'Relacionados'.
     * No Modelo pai deve existir um mutator getRelated<plural da classe relacionada>Attribute, que retorna um array.
     */
    public function syncManyToMany()
    {
        $request = request();
        if (isset($this->manyToMany)) {
            foreach ($this->manyToMany as $relationship) {
                $ids = $request->input('related' . ucwords($relationship));
                if (empty($ids)) {
                    $ids = [];
                } elseif (!is_array($ids)) {
                    $ids = [$ids];
                }
                $this->$relationship()->sync($ids);
            }
        }
    }
}